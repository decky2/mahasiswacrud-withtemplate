<?php

namespace App\DataTables;

use App\Models\MahasiswaModel;
use Livewire\Livewire;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MahasiswaDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'mahasiswa.action')
            ->addIndexColumn()
            ->addColumn('action', function(MahasiswaModel $val){
                return Livewire::mount('pages.admin.mahasiswa.mahasiswa-table-action', ['mahasiswa' => $val]);
            })
            ->editColumn('gender', function(MahasiswaModel $val) {
                return $val->gender == "P" ? "Perempuan" : "Laki-Laki";
            })
            ->editColumn('photo', function(MahasiswaModel $val){
                if($val->photo == null){
                    return "<img src='" . asset('assets/media/avatars/blank.png') . "' class='rounded-circle object-fit-cover' width='50' height='50' />";
                } else{
                    return "<img src='" . asset('storage/' . $val->photo) . "' class='rounded-circle object-fit-cover' width='50' height='50' />";
                }
            })
            ->rawColumns(['photo', 'action'])
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(MahasiswaModel $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('mahasiswa-table')
            ->columns($this->getColumns())
            ->minifiedAjax(script: "
                data._token = '" . csrf_token() . "';
                data._p = 'POST';
            ")
            ->dom('rt' . "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",)
            ->addTableClass('table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer text-gray-600 fw-semibold')
            ->setTableHeadClass('text-start text-muted fw-bold fs-7 text-uppercase gs-0')
            ->orderBy(2)
            ->drawCallbackWithLivewire(file_get_contents(resource_path('js/custom/table/_init.js')))
            ->select(false)
            ->buttons([]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::computed('action')
                ->title("Action")
                ->exportable(false)
                ->printable(false)
                ->width(50)
                ->addClass('text-center'),
            Column::make('photo')->title('Profile')->addClass('text-center'),
            Column::make('name')->title('Nama Lengkap')->addClass('text-center')->searchable(true),
            Column::make('nim')->title('NIM')->addClass('text-center')->searchable(true),
            Column::make('gender')->title('Jenis Kelamin')->addClass('text-center')->searchable(true),
            Column::make('place_birth')->title('Tempat Lahir')->addClass('text-center')->searchable(true),
            Column::make('date_birth')->title('Tanggal Lahir')->addClass('text-center'),
            Column::make('email')->addClass('text-center'),
            Column::make('phone')->title('Nomor Telepon')->addClass('text-center'),
            Column::make('address')->title('Alamat')->addClass('text-center'),  
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Mahasiswa_' . date('YmdHis');
    }
}
