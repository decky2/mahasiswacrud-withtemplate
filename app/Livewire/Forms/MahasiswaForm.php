<?php

namespace App\Livewire\Forms;

use Livewire\Attributes\Rule;
use Livewire\Form;
use App\Models\MahasiswaModel;
use Illuminate\Validation\Rule as ValidationRule;
use Illuminate\Support\Facades\Storage;
use App\Livewire\Pages\Admin\Mahasiswa\Exception;
use Livewire\Features\SupportFileUploads\WithFileUploads;


class MahasiswaForm extends Form
{
    #[Rule('required|max:255')]
    public $name = "";
    #[Rule('required|max:10|unique:mahasiswa,nim')]
    public $nim = "";
    #[Rule('required|max:1')]
    public $gender = "";
    #[Rule('required|max:100')]
    public $place_birth = "";
    #[Rule('required|date')]
    public $date_birth = "";
    #[Rule('required|email|max:100|unique:mahasiswa,email')]
    public $email = "";
    #[Rule('required|max:13')]
    public $phone = "";
    #[Rule('required|max:255|unique:mahasiswa,phone')]
    public $address = "";
    #[Rule('nullable|image|max:2048')]
    public $photo = "";

    public $id;
    public $photo_new = "";

    public function save()
    {
        // $mahasiswa = new MahasiswaModel();

        $validated = $this->validate();

        if ($this->photo) {
            $filename = $this->name;
            $filename = uniqid($filename . '_', true) . '.' . $this->photo->extension();
            $this->photo->storeAs('public/photos', $filename);
            $validated['photo'] = 'photos/' . $filename;
        }

        

        MahasiswaModel::create($validated);
        $this->reset();
    }

    public function delete($id){
        $mahasiswa = MahasiswaModel::find($id);
        unlink('storage/'.$mahasiswa->photo);
        $mahasiswa->delete();
    }

    public function edit($id){
        $mahasiswa = MahasiswaModel::find($id);
        $this->id = $id;
        $this->name = $mahasiswa->name;
        $this->nim = $mahasiswa->nim;
        $this->gender = $mahasiswa->gender;
        $this->place_birth = $mahasiswa->place_birth;
        $this->date_birth = $mahasiswa->date_birth;
        $this->email = $mahasiswa->email;
        $this->phone = $mahasiswa->phone;
        $this->address = $mahasiswa->address;
        $this->photo = $mahasiswa->photo;
        $this->id = $mahasiswa->id;
    }

    public function update()
    {
        $validated = $this->validate([
            'name' => 'required',
            'nim' => [
                'required', 'max:10',
                ValidationRule::unique('mahasiswa', 'nim')->ignore($this->id)
            ],
            'gender' => 'required',
            'place_birth' => 'required',
            'date_birth' => 'required',
            'email' => [
                'required', 'min:10', 'max:255',
                ValidationRule::unique('mahasiswa', 'email')->ignore($this->id),
            ],
            'phone' => [
                'required', 'max:13',
                ValidationRule::unique('mahasiswa', 'phone')->ignore($this->id),
            ],
            'address' => 'required',
            'photo_new' => 'nullable|max:2048',
        ]);

        if ($this->photo_new !== null) {
            if ($this->photo !== "") {
                Storage::delete('public/' . $this->photo);
            }
            try {
                $filename = $this->name;
                $filename = uniqid($filename . '_', true) . '.' . $this->photo_new->extension();
                $this->photo_new->storeAs('public/photos', $filename);
                $validated['photo'] = 'photos/' . $filename;
            } catch (Exception $e) {
                return false; 
            }
        }
        MahasiswaModel::find((string) $this->id)->update($validated);
        $this->reset();
    }

}
