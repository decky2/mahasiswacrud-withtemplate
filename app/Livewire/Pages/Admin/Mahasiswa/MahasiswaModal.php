<?php

namespace App\Livewire\Pages\Admin\Mahasiswa;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\MahasiswaModel;
use App\Livewire\Forms\MahasiswaForm;
use Livewire\Attributes\On;


class MahasiswaModal extends Component
{

    use WithFileUploads;
    public MahasiswaForm $form;

    public function store(){        
        $this->form->save();
        $this->dispatch("mahasiswa-added");
    }
    
    #[On('delete')] 
    public function delete($id){
        $this->form->delete($id);
        $this->dispatch('mahasiswa-deleted');
    }

    #[On('edit')]
    public function edit($id){
        $this->form->edit($id);    
        $this->dispatch("mahasiswa-edit");
    }

    public function update()
    {
        $this->form->update();
        $this->dispatch("mahasiswa-updated");
    }
    
    public function resetForm(){
        $this->form->reset();
        $this->form->resetValidation();
    }

    public function render()
    {
        return view('livewire.pages.admin.mahasiswa.mahasiswa-modal', [
            'parents' => MahasiswaModel::all(),
        ]);
    }


    
}
