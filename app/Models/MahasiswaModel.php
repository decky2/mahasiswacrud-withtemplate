<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MahasiswaModel extends Model
{
    use HasFactory;

    protected $table = 'mahasiswa';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'nim',
        'gender',
        'place_birth',
        'date_birth',
        'email',
        'phone',
        'address',
        'photo',
    ];
}
