<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\MahasiswaDataTable;


class MahasiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:admin-mahasiswa');
    }
    

    public function index(MahasiswaDataTable $datatable){
        return $datatable->render('pages.admin.mahasiswa.index');
    }
}
