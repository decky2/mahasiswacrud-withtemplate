<x-layouts.app>
    <x-slot:title>Mahasiswa</x-slot:title>
    <livewire:pages.admin.mahasiswa.mahasiswa-modal />

    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1">
                    <span class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></span>
                    <input type="text" data-table-id="mahasiswa-table" data-action-search
                        class="form-control form-control-solid w-300px ps-13" placeholder="Search Mahasiswa"
                        id="mahasiswa-table-search"/>
                </div>
                <div class="d-flex align-items-center my-1 px-3">
                    <x-atoms.select name="filter_by" id="filter-by-select" class="form-control form-control-solid w-250px">
                        <option value="">Pilih Pencarian</option>
                        <option value="2">Nama Lengkap</option>
                        <option value="3">NIM</option>
                        <option value="5">Tempat Lahir</option>
                    </x-atoms.select>
                </div>
                <div class="d-flex align-items-center my-1 px-3">
                    <x-atoms.select name="gender_filter" id="gender-filter-select" class="form-control form-control-solid w-250px">
                        <option value="">Pilih Jenis Kelamin</option>
                        <option value="L">Laki-laki</option>
                        <option value="P">Perempuan</option>
                    </x-atoms.select>
                </div>
            </div>

            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                    <!--begin::Add user-->
                    @can('admin-mahasiswa-create')
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#add-mahasiswa_modal">
                            <i class="ki-duotone ki-plus fs-2"></i>
                            <span>Add Mahasiswa</span>
                        </button>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card-body py-4">
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
        </div>
    </div>
    
    @push('css')
        <link rel="stylesheet" href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}">
        {{-- <link rel="stylesheet" href="https://cdn.datatables.net/2.0.0/css/dataTables.bootstrap5.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css"> --}}
    @endpush

    @push('scripts')
        {{ $dataTable->scripts() }}
        <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
        <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
        <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready(() => {
                let keyword;
                let searchByIndex;
                let genderFilter;

                // Handle search and filter-by changes
                $('[data-action-search], #filter-by-select').on('keyup change', function() {
                    clearTimeout($.data(this, 'timer'));
                    $(this).data('timer', setTimeout(searchBy, 500));
                });

                $('#gender-filter-select').on('change', function() {
                    genderFilter = $(this).val();
                    searchBy();
                });

                function searchBy() {
                    const table = $('#mahasiswa-table').DataTable();
                    keyword = $('[data-action-search]').val();
                    searchByIndex = $('#filter-by-select').val();

                    if (keyword == '') {
                        table.columns().search('').draw();
                    } else {
                        table.columns(searchByIndex).search(keyword).draw();
                    }

                    if (genderFilter != '') {
                        table.column(4).search(genderFilter).draw();
                    }
                }
            });
        </script>

    
    @endpush

</x-layouts.app>
