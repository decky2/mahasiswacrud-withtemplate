<div>
  {{-- Add --}}
  <x-mollecules.modal id="add-mahasiswa_modal" action="store" wire:ignore.self>
    <x-slot:title>Add Mahasiswa</x-slot:title>
    <x-slot:iconClose>
      <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close" wire:click="resetForm">
        <i class="ki-outline ki-cross fs-2"></i>
      </div>
    </x-slot:iconClose>
    <div class="">
      <div class="mb-6">
        <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
        <x-atoms.input name="name" wire:model='form.name' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>NIM</x-atoms.form-label>
        <x-atoms.input name="nim" wire:model='form.nim' type="number" />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
        <select wire:model='form.gender' class="form-select " data-control="select2" data-hide-search="true"
          data-placeholder="Pilih Jenis Kelamin" name="gender">
          <option value="">Jenis Kelamin</option>
          <option value="L">Laki-laki</option>
          <option value="P">Perempuan</option>
        </select>
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
        <x-atoms.input name="place_birth" wire:model='form.place_birth' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
        <x-atoms.input name="date_birth" type="date" wire:model='form.date_birth' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Email</x-atoms.form-label>
        <x-atoms.input name="email" type="email" wire:model='form.email' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Nomor Telepon</x-atoms.form-label>
        <x-atoms.input name="phone" wire:model='form.phone' type="number" />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Alamat</x-atoms.form-label>
        <x-atoms.textarea name="address" wire:model='form.address' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Foto Profil</x-atoms.form-label>
        <x-atoms.input name="photo" type="file" wire:model='form.photo' accept="image/*"/>
      </div>
      <x-slot:footer>
        <button type="button" class="btn btn-light" data-bs-dismiss="modal" wire:click="resetForm">Close</button>
        <button class="btn-primary btn" type="submit">Submit</button>
      </x-slot:footer>
    </div>
  </x-mollecules.modal>

  {{-- Edit --}}
  <x-mollecules.modal id="edit-mahasiswa_modal" action="update" wire:ignore.self>
    <x-slot:title>Edit Mahasiswa</x-slot:title>
    <x-slot:iconClose>
      <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close" wire:click="resetForm">
        <i class="ki-outline ki-cross fs-2"></i>
      </div>
    </x-slot:iconClose>
    <div class="">
      <div class="d-flex justify-content-center">
        <img src="{{ asset('storage/'.$form->photo) }}" 
        alt="Foto Profil" class='rounded-circle object-fit-cover' width='100' height='100'>
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
        <x-atoms.input name="name" wire:model='form.name' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>NIM</x-atoms.form-label>
        <x-atoms.input name="nim" wire:model='form.nim' type="number" />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
        <x-atoms.select wire:model="form.gender" class="form-select " data-control="select2" data-hide-search="true"
          data-placeholder="Pilih Jenis Kelamin" name="gender">
          <option value="">Jenis Kelamin</option>
          <option value="L">Laki-laki</option>
          <option value="P">Perempuan</option>
        </x-atoms.select>
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
        <x-atoms.input name="place_birth" wire:model='form.place_birth' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
        <x-atoms.input name="date_birth" type="date" wire:model='form.date_birth' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Email</x-atoms.form-label>
        <x-atoms.input name="email" type="email" wire:model='form.email' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Nomor Telepon</x-atoms.form-label>
        <x-atoms.input name="phone" wire:model='form.phone' type="number" />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Alamat</x-atoms.form-label>
        <x-atoms.textarea name="address" wire:model='form.address' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label>Foto Profil</x-atoms.form-label>
        <x-atoms.input name="photo_new" type="file" wire:model='form.photo_new' accept="image/*"/>
      </div>
      <x-slot:footer>
        <button type="button" class="btn btn-light" data-bs-dismiss="modal" wire:click="resetForm">Close</button>
        <button class="btn-primary btn" type="submit">Submit</button>
      </x-slot:footer>
    </div>
  </x-mollecules.modal>

</div>

@push('scripts')
  <script>
    document.addEventListener('livewire:initialized', () => {
      function refreshTable() {
        window.LaravelDataTables['mahasiswa-table'].ajax.reload();
      };
      @this.on('mahasiswa-added', () => {
        $('#add-mahasiswa_modal').modal('hide');
        refreshTable();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Data Mahasiswa berhasil ditambahkan',
          showConfirmButton: true,
          willClose: () => {
            location.reload();
          }
        });
      });
      @this.on('mahasiswa-deleted', () => {
        refreshTable();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Daa Mahasiswa berhasil dihapus',
          showConfirmButton: true,
          willClose: () => {
            location.reload();
          }
        });
      });
      @this.on('mahasiswa-edit', () => {
        $('#edit-mahasiswa_modal').modal('show');
        refreshTable();
      });
      @this.on('mahasiswa-updated', () => {
        $('#edit-mahasiswa_modal').modal('hide');
        refreshTable();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Data Mahasiswa berhasil diubah',
          showConfirmButton: true,
          willClose: () => {
            location.reload();
          }
        });
      });      
    });
  </script>
@endpush
